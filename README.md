nanohack
========

A tiny roguelike in Lua -- written from scratch, in less than 24
hours, while learning Lua :)

Presently not feature-complete, but I'm willing to call a Minimum
Viable Product and kick it out the door. It's randomized, is winnable,
and can kill you; what else do you need from a roguelike?

The good people of [r/Lua](http://www.reddit.com/r/lua/) were kind
enough to provide very helpful feedback, so I'm slowly cleaning up the
initial, sloppy, un-idiomatic, written-in-a-day codebase. Perhaps it
can become a useful learning tool or benchmark for personal coding
challenges.


Controls
--------

Upon starting the game, you will be asked to name your
adventurer. After this, you descend into the first level of the
dungeon and begin your adventure.

The following commands are available:

* n, s, e, w: Go north, south, east, or west (if possible)
* u, d: Go up, down (if possible)
* a: Attack
* p: Pick up items
* x: Wait one turn
* q: Quit

You regain one hitpoint for every turn you wait, but your final score
is affected by how many turns you took.
